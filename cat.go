package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strings"
	"time"
)

func main() {
	err := run()
	if err != nil {
		log.Fatal(err)
	}
}

func run() error {
	videoDir := flag.String("video", "", "source video dir to be combined")
	workDir := flag.String("work", ".", "directory to work in and produce file file")
	run := flag.Bool("run", false, "run the command directly")

	first := flag.String("first", "", "first video file to process")
	last := flag.String("last", "", "last video file to process")

	flag.Parse()

	if *videoDir == "" || *workDir == "" {
		return fmt.Errorf("work or video dir empty")
	}
	f, err := os.Open(*videoDir)
	if err != nil {
		return err
	}
	names, err := f.Readdirnames(-1)
	f.Close()
	if err != nil {
		return err
	}
	tmpFolder := filepath.Join(*workDir, "tmp")
	os.Mkdir(tmpFolder, 0700)
	sort.Strings(names)
	args := []string{
		"-tmp", tmpFolder,
	}
	foundFirst, foundLast := false, false
	for _, n := range names {
		if !strings.HasSuffix(strings.ToLower(n), ".mp4") {
			continue
		}
		if len(*first) > 0 && !foundFirst && *first != n {
			continue
		}
		foundFirst = true
		vp := filepath.Join(*videoDir, n)
		if !*run {
			vp = `"` + vp + `"`
		}
		args = append(args, "-cat", vp)
		if len(*last) > 0 && *last == n {
			foundLast = true
			break
		}
	}
	if len(*first) > 0 && !foundFirst {
		return fmt.Errorf("unable to find first filename %q", *first)
	}
	if len(*last) > 0 && !foundLast {
		return fmt.Errorf("unable to find last filename %q", *last)
	}
	args = append(args, fmt.Sprintf(filepath.Join(*workDir, "final_%s.mp4"), time.Now().Format("2006-01-02")))

	if *run {
		cmd := exec.Command("MP4Box", args...)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		return cmd.Run()
	}
	fmt.Println("MP4Box", strings.Join(args, " "))
	return nil
}
